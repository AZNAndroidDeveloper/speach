package uz.azn.lesson28homework

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import uz.azn.lesson28homework.databinding.ActivityMainBinding
import uz.azn.lesson28homework.main.MainFragment

class MainActivity : AppCompatActivity() {
    val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val fragment = MainFragment()
        supportFragmentManager.beginTransaction().add(R.id.frameLayout,fragment).commit()
    }
}