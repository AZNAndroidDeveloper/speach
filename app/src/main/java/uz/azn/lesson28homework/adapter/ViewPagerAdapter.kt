package uz.azn.lesson28homework.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class ViewPagerAdapter(val fragmentManager: FragmentManager):FragmentPagerAdapter(fragmentManager) {
   val fragmentlist:MutableList<Fragment> = arrayListOf()
    val titleList:MutableList<String> = arrayListOf()
    override fun getItem(position: Int): Fragment  = fragmentlist[position]

    override fun getCount(): Int  = fragmentlist.size

    override fun getPageTitle(position: Int): CharSequence?  = titleList[position]

    fun addFragment(fragment:Fragment,text:String){
        fragmentlist.add(fragment)
        titleList.add(text)
    }
}