package uz.azn.lesson28homework.hearFragment

import android.content.Intent
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.text.InputType
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import uz.azn.lesson28homework.R
import uz.azn.lesson28homework.databinding.FragmentHearBinding
import uz.azn.lesson28homework.databinding.FragmentSpeakBinding
import java.util.*

class HearFragment : Fragment(R.layout.fragment_hear), TextToSpeech.OnInitListener {
    lateinit var binding: FragmentHearBinding
    lateinit var textToSpeech: TextToSpeech
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentHearBinding.bind(view)
        textToSpeech = TextToSpeech(context, this)

        binding.buttonSay.setOnClickListener {
            if (binding.editText.text!!.isNotEmpty()) {
                convertTextIntoVoice()


            }
        }
    }

    private fun convertTextIntoVoice() {
        val text = binding.editText.text.toString()
        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null)

    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            val res = textToSpeech.setLanguage(Locale.GERMANY)
            if (res == TextToSpeech.LANG_MISSING_DATA || res == TextToSpeech.LANG_NOT_SUPPORTED) {
                Toast.makeText(activity!!.applicationContext, "", Toast.LENGTH_SHORT).show()
            } else {
                convertTextIntoVoice()
            }
        }

    }

    override fun onDestroyView() {
        textToSpeech.stop()
        textToSpeech.shutdown()

        super.onDestroyView()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

    }
}