package uz.azn.lesson28homework.main

import android.app.Activity
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.view.ViewGroup
import uz.azn.lesson28homework.R
import uz.azn.lesson28homework.adapter.ViewPagerAdapter
import uz.azn.lesson28homework.databinding.FragmentMainBinding
import uz.azn.lesson28homework.hearFragment.HearFragment
import uz.azn.lesson28homework.speakFragment.SpeakFragment


class MainFragment( ) : Fragment(R.layout.fragment_main) {
   lateinit var binding :FragmentMainBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMainBinding.bind(view)

        val viewPagerAdapter =ViewPagerAdapter(activity!!.supportFragmentManager)
        viewPagerAdapter.addFragment(HearFragment(),"Text to voice")
        viewPagerAdapter.addFragment(SpeakFragment(),"Voice to Text")
        binding.viewpager.adapter = viewPagerAdapter
        binding.tableLayout.setupWithViewPager(binding.viewpager)
        setMarginOnTabItems()
    }

    fun setMarginOnTabItems(){
            val tabItem = (binding.tableLayout.getChildAt(0) as ViewGroup).getChildAt(0)
            val tabItem2 = (binding.tableLayout.getChildAt(0) as ViewGroup).getChildAt(1)
            val params = tabItem.layoutParams as ViewGroup.MarginLayoutParams
            val params2 = tabItem2.layoutParams as ViewGroup.MarginLayoutParams
            params.setMargins(160, 48, 0, 48)
            params2.setMargins(0,48,160,48)
        }

}