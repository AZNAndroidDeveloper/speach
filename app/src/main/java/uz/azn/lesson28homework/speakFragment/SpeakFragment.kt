package uz.azn.lesson28homework.speakFragment

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import android.speech.tts.TextToSpeech
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import uz.azn.lesson28homework.R
import uz.azn.lesson28homework.databinding.FragmentSpeakBinding
import java.util.*

class SpeakFragment : Fragment(R.layout.fragment_speak),TextToSpeech.OnInitListener {

    val REQUEST_CODE_VOICE = 123
    lateinit var binding:FragmentSpeakBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentSpeakBinding.bind(view)
        binding.imageVoice.setOnClickListener {
            val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
            intent.putExtra(
                RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
            )
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speak now!")
            try {
                startActivityForResult(intent, REQUEST_CODE_VOICE)
            } catch (e: ActivityNotFoundException) {
                e.printStackTrace()
                Toast.makeText(
                    activity!!.applicationContext,
                    "Your device does not support STT.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onInit(status: Int) {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(requestCode){
            REQUEST_CODE_VOICE->{
                if (resultCode == Activity.RESULT_OK && data !=null){
                    val result  = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                    if (result!!.isNotEmpty()){
                        val recognizedText = result[0]
                        binding.voiceText.setText(recognizedText)

                    }
                }

            }
        }
    }

}